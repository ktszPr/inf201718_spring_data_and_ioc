package com.cgm.academy.address.service;

import com.cgm.academy.address.domain.Address;
import com.cgm.academy.address.domain.AddressType;
import com.cgm.academy.address.repository.AddressRepository;
import com.cgm.academy.person.repository.PersonRepository;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;


import java.util.List;

@Slf4j
public class AddressService {
    private AddressRepository addressRepository;
    // wstrzykiwanie przez setter
    public void setAddressRepository(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public List<Address> getAllAddress() {
        // Lists pochodzi z biblioteki Google Guava - całkiem przydatne narzędzie :)
        return Lists.newArrayList(addressRepository.findAll());
    }


    public void save(Address address) {
        log.info("Saving address: {}", address);
        addressRepository.save(address);
    }

}
