package com.cgm.academy.address.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name="Adres")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;


    @Temporal(TemporalType.DATE)
    private LocalDate stopDate;
    @Temporal(TemporalType.DATE)
    private LocalDate startDate;

    @NotNull
    private String street;
    @NotNull
    private String homeNo;
    private String email;
    private String country;
    private String phoneNumber;
    private String city;
    private String flatNo;
    private AddressType addressType;
    private String postalCode;




    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", street='" + street +
                ", homeNo='" + homeNo +
                ", email=" + email +
                ", country=" + country +
                ", phoneNumber=" + phoneNumber +
                ", city=" + city +
                ", flatNo=" + flatNo +
                ", addressType=" + addressType.toString() +
                ", postalCode=" + postalCode +
                '}';
    }
}