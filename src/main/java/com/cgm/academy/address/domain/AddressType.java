package com.cgm.academy.address.domain;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public enum AddressType {
    REGISTERED, TEMPORARY, WORK;


    private Integer id;
    private String name;


}
