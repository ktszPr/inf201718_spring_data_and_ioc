package com.cgm.academy.address;

import com.cgm.academy.address.domain.Address;
import com.cgm.academy.address.repository.AddressRepository;
import com.cgm.academy.address.service.AddressService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ImportResource;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.mockito.Mockito.when;


@SpringBootTest
@ImportResource("classpath:test-config.xml")
public class AddressTest {
    @Mock
    AddressRepository repositoryMock;


    @Test
    public void shouldFindAllAddresses(){
        AddressService testService = new AddressService();
        testService.setAddressRepository(repositoryMock);
        ArrayList<Address> testList = new ArrayList<Address>();
        Address a1 = new Address();
        Address a2 = new Address();
        a1.setId(1L);
        a2.setId(2L);
        a1.setStreet("Dzwonkowa");
        a1.setHomeNo("15");
        a2.setStreet("Nowa");
        a2.setHomeNo("32");
        testList.add(a1);
        testList.add(a2);

        when(repositoryMock.findAll()).thenReturn(testList);

        Assert.assertTrue(true);
    }
}
